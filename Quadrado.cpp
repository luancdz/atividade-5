#include "stdafx.h"
#include "Quadrado.h"
#include <iostream>
#include <string>

using namespace std;
Quadrado::Quadrado()
{
	this->nome = "Quadrado";
	this->numLados = 4;
}


Quadrado::~Quadrado()
{
}

void Quadrado::desenha()
{
	cout << "\t\t------------" << endl;
	for (int i = 0; i < 5; i++) {
		cout << "\t\t|          |" << endl;
	}
	cout << "\t\t------------" << endl;
}

void Quadrado::calculaArea()
{
}

void Quadrado::imprime()
{
	cout << "Nome: " << this->nome << endl;
	cout << "Numero de lados: " << this->numLados << endl;
	cout << "Area total: " << this->areaTotal << endl;

}

void Quadrado::setNumLados(int numLados)
{
	this->numLados = numLados;
}

int Quadrado::getNumLados()
{
	return this->numLados;
}

void Quadrado::setNome(string nome)
{
	this->nome = nome;
}

void Quadrado::setAreaTotal(float areaTotal)
{
	this->areaTotal = areaTotal;
}

float Quadrado::getAreaTotal()
{
	return this->areaTotal;
}

string Quadrado::getNome()
{
	return this->nome;
}

void Quadrado::setTamanhoLado(float tamanho)
{
	this->tamanhoLado = tamanho;
}

float Quadrado::setTamanhoLado()
{
	return this->tamanhoLado;
}
