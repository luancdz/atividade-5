#pragma once
#include <iostream>
#include <string>
#include "Poligono.h"
using namespace std;
class Triangulo: virtual public Poligono 
{
private:
	string nome;
	int numLados;
	float areaTotal;
	float altura;
	float base;

public:
	Triangulo();
	~Triangulo();
	void calculaArea();
	void imprime();
	void setNumLados(int numLados);
	int getNumLados();
	void desenha();
	void setNome(string nome);
	void setAreaTotal(float areaTotal);
	float getAreaTotal();
	string getNome();
	float getAltura();
	float getBase();
	void getAltura(float altura);
	void getBase(float base);

};

