#pragma once
#include "Poligono.h"
#include <iostream>
#include <string>
using namespace std;
class Quadrado:virtual public Poligono
{
private:
	string nome;
	int numLados;
	float areaTotal;
	float tamanhoLado;
public:
	Quadrado();
	~Quadrado();
	void desenha();
	void calculaArea();
	void imprime();
	void setNumLados(int numLados);
	int getNumLados();
	void setNome(string nome);
	void setAreaTotal(float areaTotal);
	float getAreaTotal();
	string getNome();
	void setTamanhoLado(float tamanho);
	float setTamanhoLado();
};

