#include "stdafx.h"
#include "Triangulo.h"
#include <string>
#include <iostream>

using namespace std;
Triangulo::Triangulo()
{
	this->nome = "Triangulo";
	this->numLados = 3;
}


Triangulo::~Triangulo()
{
}

void Triangulo::calculaArea()
{
}

void Triangulo::imprime()
{
	cout << "Nome: " << this->nome << endl;
	cout << "Numero de lados: " << this->numLados << endl;
	cout << "Area total: " << this->areaTotal << endl;
}

void Triangulo::setNumLados(int numLados)
{
	this->numLados = numLados;
}

int Triangulo::getNumLados()
{
	return this->numLados;
}

void Triangulo::desenha()
{
	cout << "\t\t       ^" << endl;
	cout << "\t\t      / \\" << endl;
	cout << "\t\t     /   \\" << endl;
	cout << "\t\t    /     \\" << endl;
	cout << "\t\t   /       \\" << endl;
	cout << "\t\t  /         \\" << endl;
	cout << "\t\t /           \\" << endl;
	cout << "\t\t---------------" << endl;
}

void Triangulo::setNome(string nome)
{
	this->nome = nome;
}

void Triangulo::setAreaTotal(float areaTotal)
{
	this->areaTotal = areaTotal;
}

float Triangulo::getAreaTotal()
{
	return this->areaTotal;
}

string Triangulo::getNome()
{
	return this->nome;
}

float Triangulo::getAltura()
{
	return this->altura;
}

float Triangulo::getBase()
{
	return this->base;
}

void Triangulo::getAltura(float altura)
{
	this->altura = altura;
}

void Triangulo::getBase(float base)
{
	this->base = base;
}
