#pragma once
#include "Quadrado.h"
#include "Triangulo.h"

class TrianguloQuadrado: virtual public Quadrado,virtual public Triangulo
{
public:
	TrianguloQuadrado();
	~TrianguloQuadrado();
	void imprime();
	void desenha();
	void calculaArea();
	void setNumLados(int numLados);
	int getNumLados();
	void setNome(string nome);
	void setAreaTotal(float areaTotal);
	float getAreaTotal();
	string getNome();
};

