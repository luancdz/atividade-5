#pragma once
#include <iostream>
#include <string>
using namespace std;
class Poligono
{
private:
	string nome;
	int numLados;
	float areaTotal;
public:
	Poligono();
	~Poligono();
	virtual void desenha()=0;
	virtual void calculaArea() = 0;
	virtual void imprime() = 0;
	virtual void setNumLados(int numLados) = 0;
	virtual int getNumLados() = 0 ;
	virtual void setNome(string nome) = 0;
	virtual void setAreaTotal(float areaTotal) = 0;
	virtual float getAreaTotal() = 0;
	virtual string getNome() = 0;
};

