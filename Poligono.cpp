#include "stdafx.h"
#include "Poligono.h"
#include <iostream>
#include <string>
using namespace std;
Poligono::Poligono()
{
	
}


Poligono::~Poligono()
{
}

void Poligono::desenha()
{
}


void Poligono::calculaArea()
{
}

void Poligono::imprime()
{
	cout << "Nome: " << this->nome << endl;
	cout << "Numero de lados: " << this->numLados << endl;
	cout << "Area total: " << this->areaTotal << endl;
}

/*void Poligono::imprime()
{
	cout << "Nome: " << this->nome << endl;
	cout << "Numero de lados: " << this->numLados << endl;
	cout << "Area total: " << this->areaTotal << endl;
}*/

void Poligono::setNumLados(int numLados)
{
	this->numLados = numLados;
}

int Poligono::getNumLados()
{
	return this->numLados;
}

void Poligono::setNome(string nome)
{
	this->nome = nome;
}

void Poligono::setAreaTotal(float areaTotal)
{
	this->areaTotal = areaTotal;
}

float Poligono::getAreaTotal()
{
	return this->areaTotal;
}

string Poligono::getNome()
{
	return this->nome;
}
