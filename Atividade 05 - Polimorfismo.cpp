// Atividade 05 - Polimorfismo.cpp : Define o ponto de entrada para a aplica��o de console.
//

#include "stdafx.h"
#include <iostream>
#include "Poligono.h"
#include "Quadrado.h"
#include "Triangulo.h"
#include "TrianguloQuadrado.h"
using namespace std;
int main()
{

	Triangulo* triangulo1 = new Triangulo();
	Triangulo* triangulo2  = new Triangulo();
	Triangulo* triangulo3 = new Triangulo();

	Quadrado* quadrado1 = new Quadrado();
	Quadrado* quadrado2 = new Quadrado();
	Quadrado* quadrado3 = new Quadrado();

	//Poligono* poligono1 = new Poligono();

	Poligono* poligono[100];
	poligono[0] = triangulo1;
	poligono[1] = triangulo2;
	poligono[2] = triangulo3;
	poligono[3] = quadrado1;
	poligono[4] = quadrado2;
	poligono[5] = quadrado3;
	//poligono[6] = poligono1;

	for (int i = 0; i < 6; i++) {
		poligono[i]->imprime();
		poligono[i]->desenha();
	}

	TrianguloQuadrado* trianguloQuadrado = new TrianguloQuadrado();
	trianguloQuadrado->imprime();
	

	getchar();
	return 0;
}

